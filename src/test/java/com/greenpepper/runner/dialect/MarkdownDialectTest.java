package com.greenpepper.runner.dialect;

import com.greenpepper.runner.CommandLineRunner;
import com.greenpepper.runner.FactoryConverter;
import com.greenpepper.runner.LoggingMonitor;
import com.greenpepper.util.cli.ParseException;
import org.jsoup.Jsoup;
import org.junit.After;
import org.junit.Test;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.net.URL;

import static java.lang.String.format;
import static org.apache.commons.io.FileUtils.deleteQuietly;
import static org.apache.commons.io.FileUtils.toFile;
import static org.apache.commons.io.IOUtils.toInputStream;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class MarkdownDialectTest {

    @After
    public void tearDown() {
        URL resource = getClass().getResource("/MarkDownSyntax.md.shouldSupportTheDialectOption.html");
        if (resource != null) {
            deleteQuietly(toFile(resource));
        }
    }

    @Test
    public void convertBlank() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect();
        assertTrue(isBlank(dialect.convert(toInputStream(""))));
    }

    @Test
    public void convertTitles() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect();
        String input = "# Title 1\n\n" +
                "## Title 1.1\n\n" +
                "## Title 1.2";
        String convert = dialect.convert(toInputStream(input));
        assertTrue(convert.contains("<h1>Title 1</h1>"));
        assertTrue(convert.contains("<h2>Title 1.1</h2>"));
        assertTrue(convert.contains("<h2>Title 1.2</h2>"));
    }

    @Test
    public void convertSingleHeaderTable() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect();
        String input = "| rule for | MyFixture |\n" +
                "|-----|-----|----|\n" +
                "| a | b | sum() |\n" +
                "| 1 | 2 | 3 |";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/body/table/thead/tr[1]/th[1]", equalTo("rule for")) );
        assertThat(xml, hasXPath("/html/body/table/thead/tr[1]/th[2]", equalTo("MyFixture")) );
        assertThat(xml, hasXPath("/html/body/table/tbody/tr[1]/td[1]", equalTo("a")) );
        assertThat(xml, hasXPath("/html/body/table/tbody/tr[1]/td[2]", equalTo("b")) );
        assertThat(xml, hasXPath("/html/body/table/tbody/tr[1]/td[3]", equalTo("sum()")) );
    }

    @Test
    public void convertMultiHeaderTable() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect();
        String input = "| rule for | MyFixture |\n" +
                "| a | b | sum() |\n" +
                "|-----|-----|----|\n" +
                "| 1 | 2 | 3 |";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/body/table/thead/tr[1]/th[1]", equalTo("rule for")) );
        assertThat(xml, hasXPath("/html/body/table/thead/tr[1]/th[2]", equalTo("MyFixture")) );
        assertThat(xml, hasXPath("/html/body/table/thead/tr[2]/th[1]", equalTo("a")) );
        assertThat(xml, hasXPath("/html/body/table/thead/tr[2]/th[2]", equalTo("b")) );
        assertThat(xml, hasXPath("/html/body/table/thead/tr[2]/th[3]", equalTo("sum()")) );
    }

    @Test
    public void convertSingleLineTable() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect();
        String input = "| Begin |\n|-----|\n";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/body/table/thead/tr[1]/th[1]", equalTo("Begin")) );
    }

    @Test
    public void shouldSupportTheDialectOption() throws IOException, ParseException {
        File spec = toFile(getClass().getResource("/MarkDownSyntax.md"));
        CommandLineRunner runner = new CommandLineRunner();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ByteArrayOutputStream err = new ByteArrayOutputStream();

        LoggingMonitor monitor = new LoggingMonitor( new PrintStream(out), new PrintStream(err) );
        runner.setMonitor(monitor);

        runner.run("--debug", "-d", MarkdownDialect.class.getName(), spec.getAbsolutePath(),
                format("%s.shouldSupportTheDialectOption.html", spec.getAbsolutePath()));

        String output = out.toString();
        assertThat(output, containsString("Running MarkDownSyntax.md"));
        assertThat(output, containsString("5 tests: 5 right, 0 wrong, 0 ignored, 0 exception(s)"));
    }

    @Test
    public void shouldInsertDefaultCSS() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect();
        String input = "| Begin info |\n|-----|\n";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/head/style", containsString("table > thead > tr > th ")) );
        assertThat(xml, hasXPath("/html/head/style", containsString("table > tbody > tr > td ")) );
    }

    @Test
    public void shouldInsertCustomCSSFromClassPath() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect("com/greenpepper/runner/dialect/test.css");
        String input = "| Begin info |\n|-----|\n";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/head/style", containsString("body { background-color: blue; }")) );
    }

    @Test
    public void shouldInsertCustomCSSFromFileURL() throws Exception {
        URL resource = getClass().getResource("test.css");
        String protocol = resource.getProtocol();
        String path = format("%s://%s", protocol, resource.getFile());
        MarkdownDialect dialect = new MarkdownDialect(path);
        String input = "| Begin info |\n|-----|\n";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/head/style", containsString("body { background-color: blue; }")) );
    }

    @Test
    public void shouldInsertCustomJSFromFileURL() throws Exception {
        URL resource = getClass().getResource("test.js");
        String protocol = resource.getProtocol();
        String path = format("%s://%s", protocol, resource.getFile());
        MarkdownDialect dialect = new MarkdownDialect("", path);
        String input = "| Begin info |\n|-----|\n";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/head/script", containsString("function test() {  alert(\"testing\"); }")) );
        assertThat("Should contain the default css",
                xml, hasXPath("/html/head/style", containsString("table > thead > tr > th ")) );
    }

    @Test
    public void shouldInsertCustomJSFromClasspath() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect("", "com/greenpepper/runner/dialect/test.js");
        String input = "| Begin info |\n|-----|\n";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/head/script", containsString("function test() {  alert(\"testing\"); }")) );
        assertThat("Should contain the default css",
                xml, hasXPath("/html/head/style", containsString("table > thead > tr > th ")) );
    }

    @Test
    public void shouldInsertCustomJSandCSSFromClasspath() throws Exception {
        MarkdownDialect dialect = new MarkdownDialect("com/greenpepper/runner/dialect/test.css",
                "com/greenpepper/runner/dialect/test.js");
        String input = "| Begin info |\n|-----|\n";
        String convert = dialect.convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/head/script", containsString("function test() {  alert(\"testing\"); }")) );
        assertThat(xml, hasXPath("/html/head/style", containsString("body { background-color: blue; }")) );
    }

    @Test
    public void testInstanciationByFactoryConverter() throws Exception {
        FactoryConverter factoryConverter = new FactoryConverter();
        Object dialect = factoryConverter.convert("com.greenpepper.runner.dialect.MarkdownDialect;" +
                "com/greenpepper/runner/dialect/test.css;com/greenpepper/runner/dialect/test.js");
        assertThat(dialect, instanceOf(MarkdownDialect.class));

        String input = "| Begin info |\n|-----|\n";
        String convert = ((MarkdownDialect)dialect).convert(toInputStream(input));

        Document xml = parse(convert);
        assertThat(xml, hasXPath("/html/head/script", containsString("function test() {  alert(\"testing\"); }")) );
        assertThat(xml, hasXPath("/html/head/style", containsString("body { background-color: blue; }")) );
    }

    private static Document parse(String xml) throws Exception {
        org.jsoup.nodes.Document document = Jsoup.parse(xml);
        document.getElementsByTag("META").remove();
        xml = document.toString();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(false);
        documentBuilderFactory.setValidating(false);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        return documentBuilder.parse(new ByteArrayInputStream(xml.getBytes()));
    }

}