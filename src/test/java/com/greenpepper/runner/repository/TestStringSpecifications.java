package com.greenpepper.runner.repository;

public class TestStringSpecifications {
    
    public static final String SimpleAlternateCalculatorTest = 
        "<html><table border='1' cellspacing='0'>" +
        "<tr><td colspan='2'>com.greenpepper.testing.AlternateCalculator</td></tr>" +
        "<tr><td>a</td><td>b</td><td>sum()</td></tr>" + 
        "<tr><td>1</td><td>2</td><td>3</td></tr>" +
        "<tr><td>2</td><td>3</td><td>15</td></tr>" +
        "<tr><td>2</td><td>3</td><td>a</td></tr>" +
        "</table></html>";

	public static final String SimpleAlternateCalculatorXmlReport =
			"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><documents><document><time-statistics><execution>0</execution><total>0</total></time-statistics><statistics><success>0</success><failure>0</failure><error>0</error><ignored>0</ignored></statistics><results><![CDATA[<html><head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\"></head><body><script>(function(window) {\n" +
					"    window.greenpepperJS = {\n" +
					"        toggleStackTrace: toggleStackTrace\n" +
					"    };\n" +
					"\n" +
					"    function toggleStackTrace(button) {\n" +
					"        var stacktraces = button.parentElement.getElementsByClassName(\"greenpepper-report-stacktrace\");\n" +
					"        for (var i = 0; i < stacktraces.length; i++) {\n" +
					"            var stacktrace = stacktraces[i];\n" +
					"            var isShown = stacktrace.classList.toggle(\"show\");\n" +
					"            if (isShown) {\n" +
					"                button.textContent = 'v';\n" +
					"            } else {\n" +
					"                button.textContent = '>';\n" +
					"            }\n" +
					"\n" +
					"        }\n" +
					"    }\n" +
					"\n" +
					"})(this);</script><style type=\"text/css\">.greenpepper-report-exception>font {\n" +
					"\tfont-size: small;\n" +
					"}\n" +
					".greenpepper-report-stacktrace-toggle {\n" +
					"\tbackground: none;\n" +
					"\tborder: 1px dashed black;\n" +
					"\tborder-radius: 50%;\n" +
					"\tpadding: 6px 12px;\n" +
					"\tmargin-left: 20px;\n" +
					"\tcursor: pointer;\n" +
					"}\n" +
					".greenpepper-report-stacktrace {\n" +
					"\tfont-size: small;\n" +
					"\tdisplay: none;\n" +
					"\tz-index: 1;\n" +
					"\tposition: absolute;\n" +
					"\toverflow: auto;\n" +
					"\tmax-height: 200px;\n" +
					"\tpadding: 10px;\n" +
					"\tmargin: 0 0 10px;\n" +
					"\tline-height: 1.42857143;\n" +
					"\tcolor: #333;\n" +
					"\tword-break: break-all;\n" +
					"\tword-wrap: break-word;\n" +
					"\tbackground-color: #f5f5f5;\n" +
					"\tborder: 1px solid #ccc;\n" +
					"\tborder-radius: 4px;\n" +
					"\tbox-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n" +
					"}\n" +
					".greenpepper-report-stacktrace.show {\n" +
					"\tdisplay: block !important;\n" +
					"}\n" +
					"</style><table border=\"1\" cellspacing=\"0\"><tbody><tr><td colspan=\"2\">com.greenpepper.testing.AlternateCalculator</td></tr><tr><td>a</td><td>b</td><td>sum()</td></tr><tr><td>1</td><td>2</td><td>3</td></tr><tr><td>2</td><td>3</td><td>15</td></tr><tr><td>2</td><td>3</td><td>a</td></tr></tbody></table></body></html>]]></results></document></documents>";
}
