# Greenpepper support for MS Docx, OpenXML document

[OpenXML documents]: https://en.wikipedia.org/wiki/Office_Open_XML

Using the `--dialect` option in greenpepper, you can define a class that will be
used to convert a specification from a syntax to HTML. 

That is what is used for greenpepper to support [OpenXML documents] and MS Word documents.


## Example usage

To use the default Markdown dialect, you will need to include `greenpepper-extensions-java` 
in your classpath. Then you need to use the class `com.greenpepper.runner.dialect.MarkdownDialect`.

```bash
java -cp greenpepper-core.jar:greenpepper-extensions-java.jar com.greenpepper.runner.Main \
         -d com.greenpepper.runner.dialect.DocxDialect \
         specification.docx \
         output.html
         
java -cp greenpepper-core.jar:greenpepper-extensions-java.jar com.greenpepper.runner.Main \
         -d com.greenpepper.runner.dialect.DocDialect \
         specification.doc \
         output.html
         
java -cp greenpepper-core.jar:greenpepper-extensions-java.jar com.greenpepper.runner.Main \
         -d com.greenpepper.runner.dialect.OdtDialect \
         specification.odt \
         output.html
```

