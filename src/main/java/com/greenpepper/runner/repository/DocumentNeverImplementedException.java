package com.greenpepper.runner.repository;

import com.greenpepper.repository.DocumentNotFoundException;

import java.net.URI;
import java.net.URL;

public class DocumentNeverImplementedException extends DocumentNotFoundException {

    public DocumentNeverImplementedException(URI location) {
        super(location);
    }

    public DocumentNeverImplementedException(URL location) {
        super(location);
    }

    public DocumentNeverImplementedException(String location) {
        super(location);
    }
}
